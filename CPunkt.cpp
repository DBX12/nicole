#include <iostream>
class Punkt{
private:	// assumed access modifier
	int _x;
	int _y;
public:
	/**
	* Konstruktor für den Punkt
	* @param x X-Koordinate des Punkts
	* @param y Y-Koordinate des Punkts
	*/
	Punkt(int x=0, int y=0){this->_x = x; this->_y = y;}
	/**
	* Gibt diesen Punkt auf der Konsole aus. Fügt außerdem einen Zeilenumbruch hinzu
	*/
	void print(){ std::cout << "(" << this->_x << " , " << this->_y << ")" << std::endl;} // also added a std::endl for new line
};
