#include "CPfad.cpp"
#include <iostream>
#include <cstdlib> // für std::srand benötigt
#include <ctime> // für die Initialisierung von std::srand
void pfadInitialisieren(Pfad* pfad);
void fragePunkt(Pfad* pfad);
int main(){
	std::cout<<"Anzahl der Punkte: "<<std::endl<<"> ";
	unsigned int anz=0;
	std::cin >> anz;
	try{
		Pfad* pfad = new Pfad(anz);
//		pfadInitialisieren(pfad);
		char c='0';
		do{
			fragePunkt(pfad);
			std::cout << "Weiteren Punkt einfügen? (Y|AnyKey)"<<std::endl<<"> ";
			std::cin >> c;
		 }while(c == 'Y' || c == 'y');
		std::cout<<"Das ist der Pfad: "<<std::endl;
		pfad->printPfad();
	}catch(char const* exception){
		std::cerr<<"Shit hit the fan. Here is why:"<<std::endl<<"\t"<<exception<<std::endl;
	}
}

void pfadInitialisieren(Pfad* pfad){
	std::srand(std::time(0));
	for(unsigned int i = 0; i < pfad->getPfadSize(); i++){
		int x = std::rand()%89+10; //Zufallszahl zwischen 10 und 99. Zwecks de Format
		int y = std::rand()%89+10;
		Punkt* p = new Punkt(x,y);
		try{
			pfad->insertKoord(*p);
		}catch(char const* exception){
			std::cerr<<"Konnte Punkt nicht einfügen:"<<std::endl<<"\t"<<exception<<std::endl;
			return;
		}
		std::cout<<"Füge Punkt ein: ( "<<x<<" , " << y << " )"<<std::endl;
	}
}

void fragePunkt(Pfad* pfad){
	std::cout<<"Bitte X Koordinate eingeben: "<<std::endl<<"> ";
	int x = 0;
	std::cin >> x;
	
	std::cout<<"Bitte Y Koordinate eingeben: "<<std::endl<<"> ";
	int y = 0;
	std::cin >> y;
	
	Punkt* p = new Punkt(x,y);
	try{
		pfad->insertKoord(*p);
	}catch(char const* exception){
		std::cerr<<"Konnte Punkt nicht einfügen:"<<std::endl<<"\t"<<exception<<std::endl;
		return;
	}
	std::cout << "Punkt eingefügt: ( " << x << " , " << y << " )"<<std::endl;
}
