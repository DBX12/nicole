#include "CPunkt.cpp"
class Pfad{
private:
	/**
	* Array aus Punkt Objekten
	*/
	Punkt* punkte;
	unsigned int anzahlPunkte;
	/**
	* Speichert die Position, an die eingefügt werden soll.
	*/
	unsigned int insertPos;
public:
	/**
	* Konstruktor für Pfad Objekt
	* @param anzPunkte Anzahl der Punkte dieses Pfades
	*/
	Pfad(unsigned int anzPunkte);
	/**
	* Dekonstruktor für das Pfad Objekt
	* Löscht den Pointer punkte
	*/
	~Pfad();
	/**
	* Fügt einen Punkt an das Ende des punkte Arrays an.
	* @param Punkt& Referenz auf den einzufügenden Punkt
	* @throws std::exception Wirft eine Ausnahme, falls über das Array hinausgeschrieben würde
	*/
	void insertKoord(Punkt& p);
	/**
	* Gibt den Punkt an der Position pos aus
	* @param pos Position des auszugebenden Punktes (1. Punkt = Position 0)
	*/
	void printKoord(unsigned int pos);
	/**
	* Gratis Bonus-Methode zum Ausgeben des gesamten Pfades
	*/
	void printPfad();
	/**
	* Getter für die Größe des Pfades
	* @return Länge des Pfades
	*/
	inline unsigned int getPfadSize(){return this->anzahlPunkte;}
};
