#include "CPfad.h"
Pfad::Pfad(unsigned int anz){
	if(anz == 0){
		throw "Anzahl an Punkte muss größer als 0 sein";
	}
	this->anzahlPunkte = anz;
	this->punkte = new Punkt[anz];
	this->insertPos = 0;
}

Pfad::~Pfad(){
	delete this->punkte;
}

void Pfad::insertKoord(Punkt& p){
	if(this->insertPos >= this->anzahlPunkte)
		throw "Der Pfad ist schon voll, es können keine weiteren Punkte eingefügt werden.";
	// füge Punkt hinten an und erhöhe den Zähler um 1
	this->punkte[this->insertPos++] = p;
}

void Pfad::printKoord(unsigned int pos){
	this->punkte[pos].print();
}

void Pfad::printPfad(){
	for(unsigned int i=0; i< this->anzahlPunkte; i++){
		this->punkte[i].print();
	}
}
